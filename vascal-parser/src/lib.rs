/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use lalrpop_util::lalrpop_mod;

pub mod ast;
pub mod visit;

lalrpop_mod!(
    #[allow(clippy::all)]
    pub syntax
);

pub fn parse_file(source: &str) -> Result<ast::File, String> {
    let parser = syntax::ParseFileParser::new();

    parser.parse(source).map_err(|e| e.to_string())
}

#[cfg(test)]
mod test_function {
    use super::*;

    fn test_parse(src: &str) -> ast::File {
        parse_file(src).unwrap()
    }

    #[test]
    fn parse_function_empty() {
        let src = r#"
            function Test(): Int
            is 0;
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_function_with_parameters() {
        let src = r#"
            function Const(a: A, _b: B): A
            is a;
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_function_with_extra_comma_in_parameters() {
        let src = r#"
            function ExtraComma(a: A, b: B,): B
            is a;
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_function_with_snake_case_name() {
        let src = r#"
            function snake_case(): Int
            is 1337;
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_function_with_complex_expression() {
        let src = r#"
            function do_thing(a: Int): Int
            is (9 - (a mod 10)) + (a / 10);
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_function_with_function_call() {
        let src = r#"
            function call_function(a: Int): Int
            is another_function(a, a + 1);
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_function_with_locals() {
        let src = r#"
            function do_expensive_thing(a: Int): Int
                tmp0: Int = expensive_function(a);
                tmp1: Int = expensive_function(a * a)
            is (tmp0 * tmp0) - tmp1;
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_function_with_if() {
        let src = r#"
            function max(a: Int): Int
            is if a > 0 then
                a
            else
                0 - a;
            "#;

        test_parse(src);
    }
}

#[cfg(test)]
mod test_procedure {
    use super::*;

    fn test_parse(src: &str) -> ast::File {
        parse_file(src).unwrap()
    }

    #[test]
    fn parse_procedure_empty() {
        let src = r#"
            procedure Proc() begin end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_single_parameter() {
        let src = r#"
            procedure Test(a: Int) begin end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_parameter_modifiers() {
        let src = r#"
            procedure TestModifiers(in a: Int, out result: Int) begin end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_stement() {
        let src = r#"
            procedure Copy(in a: Int, out b: Int)
            begin
                b := a
            end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_statement_extra_semicolon() {
        // testing extra semicolon at the end
        let src = r#"
            procedure Copy(in a: Int, out b: Int)
            begin
                b := a;
            end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_procedure_call_statement() {
        // testing procedure call statement
        let src = r#"
            procedure MyCopy(in a: Int, out b: Int)
            begin
                Copy(a, out b)
            end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_assignment_statment() {
        let src = r#"
            procedure Increment(inout a: Int)
            begin
                a := a + 1
            end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_local() {
        let src = r#"
            procedure Swap(inout a: Int, inout b: Int)
                tmp: Int = a;
            begin
                a := b;
                b := tmp;
            end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_while_loop() {
        let src = r#"
            procedure Factorial(n: Int, out result: Int)
                acc: Int = 1;
                count: Int = 1;
            begin
                while count <= n do
                begin
                    acc := acc * count;
                    count := coung + 1;
                end;

                result := acc;
            end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_parametrized_type_and_for_loop() {
        let src = r#"
            procedure Sum(vals: Array[Int], out result: Int)
                acc: Int = 0;
                len: Int = length(vals);
            begin
                for index := 0 to len do
                begin
                    acc := acc + vals[index];
                end;

                result := acc;
            end
            "#;

        test_parse(src);
    }

    #[test]
    fn parse_procedure_with_for_loop_down_to() {
        let src = r#"
            procedure Sum(vals: Array[Int], out result: Int)
                acc: Int = 0;
                len: Int = length(vals);
            begin
                for index := len - 1 down to 0 do
                begin
                    acc := acc + vals[index];
                end;

                result := acc;
            end
            "#;

        test_parse(src);
    }

}
