/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use crate::ast;
use crate::ast::Expression;

pub trait Visit {
    // File

    fn visit_file(&mut self, f: &ast::File) {
        for item in &f.items {
            self.visit_item(item);
        }
    }

    fn visit_item(&mut self, item: &ast::Item) {
        match item {
            ast::Item::Function(func) => {
                self.visit_function(func);
            }
            ast::Item::Procedure(proc) => {
                self.visit_procedure(proc);
            }
        }
    }

    // Items

    fn visit_function(&mut self, func: &ast::Function) {
        for arg in &func.arguments {
            self.visit_function_argument(arg);
        }

        self.visit_type(&func.ret_type);

        for local in &func.locals {
            self.visit_local(local);
        }

        self.visit_expression(&func.body);
    }

    fn visit_procedure(&mut self, proc: &ast::Procedure) {
        for arg in &proc.arguments {
            self.visit_procedure_argument(arg);
        }

        for local in &proc.locals {
            self.visit_local(local);
        }

        self.visit_statement(&proc.body);
    }

    // Function Item

    fn visit_function_argument(&mut self, arg: &ast::FunctionArgument) {
        self.visit_type(&arg.ty);
    }

    // Procedure Item

    fn visit_procedure_argument(&mut self, arg: &ast::ProcedureArgument) {
        self.visit_procedure_argument_modifier(arg.modifier);
        self.visit_type(&arg.ty);
    }

    fn visit_procedure_argument_modifier(&mut self, modifier: ast::ProcedureArgumentModifier) {
        let _ = modifier;
    }

    // Statement
    fn visit_statement(&mut self, stmt: &ast::Statement) {
        match stmt {
            ast::Statement::Block { statements } => self.visit_statement_block(statements),
            ast::Statement::Assignment { lhs, rhs } => self.visit_statement_assignment(lhs, rhs),
            ast::Statement::Return { expr } => self.visit_statement_return(expr),
            ast::Statement::ProcedureCall { name, arguments } => {
                self.visit_statement_procedure_call(name, arguments)
            }
            ast::Statement::If {
                cond,
                then_stmt,
                else_stmt,
            } => self.visit_statement_if(cond, then_stmt, else_stmt.as_ref().map(|x| &*(*x))),
            ast::Statement::While { cond, body } => self.visit_statement_while(cond, body),
            ast::Statement::For {
                var,
                initial_value,
                final_value,
                loop_type,
                body,
            } => self.visit_statement_for(var, initial_value, final_value, *loop_type, body),
        }
    }

    fn visit_statement_block(&mut self, stmts: &[ast::Statement]) {
        for stmt in stmts {
            self.visit_statement(stmt);
        }
    }

    fn visit_statement_assignment(&mut self, lhs: &ast::Expression, rhs: &ast::Expression) {
        self.visit_expression(lhs);
        self.visit_expression(rhs);
    }

    fn visit_statement_return(&mut self, expr: &ast::Expression) {
        self.visit_expression(expr);
    }

    fn visit_statement_procedure_call(
        &mut self,
        proc: &ast::Identifier,
        arguments: &[(Option<ast::ProcedureArgumentModifier>, Expression)],
    ) {
        let _ = proc;
        for (_, argument) in arguments {
            self.visit_expression(argument);
        }
    }

    fn visit_statement_if(
        &mut self,
        cond: &ast::Expression,
        then_stmt: &ast::Statement,
        else_stmt: Option<&ast::Statement>,
    ) {
        self.visit_expression(cond);
        self.visit_statement(then_stmt);
        if let Some(stmt) = else_stmt {
            self.visit_statement(stmt);
        }
    }

    fn visit_statement_while(&mut self, cond: &ast::Expression, body: &ast::Statement) {
        self.visit_expression(cond);
        self.visit_statement(body);
    }

    fn visit_statement_for(
        &mut self,
        var: &ast::Identifier,
        initial_value: &ast::Expression,
        final_value: &ast::Expression,
        loop_type: ast::ForLoopType,
        body: &ast::Statement,
    ) {
        let _ = var;
        let _ = loop_type;
        self.visit_expression(initial_value);
        self.visit_expression(final_value);
        self.visit_statement(body);
    }

    // Expression
    fn visit_expression(&mut self, expr: &ast::Expression) {
        match expr {
            ast::Expression::Var { name } => self.visit_expression_identifier(name),
            ast::Expression::Num { value } => self.visit_expression_num(*value),
            ast::Expression::Index { base, index } => self.visit_expression_index(base, index),
            ast::Expression::FunctionCall { base, arguments } => {
                self.visit_expression_function_call(base, arguments);
            }
            ast::Expression::Operator { op, arg_a, arg_b } => {
                self.visit_expression_operator(*op, arg_a, arg_b);
            }
            ast::Expression::IfThenElse {
                cond,
                then_expr,
                else_expr,
            } => {
                self.visit_expression_if(cond, then_expr, else_expr);
            }
        }
    }

    fn visit_expression_identifier(&mut self, ident: &ast::Identifier) {
        let _ = ident;
    }

    fn visit_expression_num(&mut self, value: u64) {
        let _ = value;
    }

    fn visit_expression_index(&mut self, base: &ast::Expression, index: &ast::Expression) {
        self.visit_expression(base);
        self.visit_expression(index);
    }

    fn visit_expression_function_call(
        &mut self,
        base: &ast::Expression,
        arguments: &[ast::Expression],
    ) {
        self.visit_expression(base);
        for arg in arguments {
            self.visit_expression(arg);
        }
    }

    fn visit_expression_operator(
        &mut self,
        op: ast::Operator,
        arg_a: &ast::Expression,
        arg_b: &ast::Expression,
    ) {
        self.visit_operator(op);
        self.visit_expression(arg_a);
        self.visit_expression(arg_b);
    }

    fn visit_expression_if(
        &mut self,
        cond: &ast::Expression,
        then_expr: &ast::Expression,
        else_expr: &ast::Expression,
    ) {
        self.visit_expression(cond);
        self.visit_expression(then_expr);
        self.visit_expression(else_expr);
    }

    // Type

    fn visit_type(&mut self, ty: &ast::Type) {
        match ty {
            ast::Type::Named(ident) => self.visit_type_named(ident),
            ast::Type::Parametrized { base, parameters } => {
                self.visit_type_parametrized(base, parameters);
            }
        }
    }

    fn visit_type_named(&mut self, ident: &ast::Identifier) {
        let _ = ident;
    }

    fn visit_type_parametrized(&mut self, base: &ast::Type, parameters: &[ast::TypeArgument]) {
        self.visit_type(base);
        for parameter in parameters {
            self.visit_type_argument(parameter);
        }
    }

    fn visit_type_argument(&mut self, arg: &ast::TypeArgument) {
        match arg {
            ast::TypeArgument::Type(ty) => self.visit_type(ty),
        }
    }

    // Local

    fn visit_local(&mut self, local: &ast::VariableDeclaration) {
        self.visit_type(&local.ty);
        self.visit_expression(&local.value);
    }

    // misc

    fn visit_operator(&mut self, op: ast::Operator) {
        let _ = op;
    }
}
