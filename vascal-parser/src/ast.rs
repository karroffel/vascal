/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Identifier(pub String);

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct File {
    pub items: Vec<Item>,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Item {
    Function(Function),
    Procedure(Procedure),
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Function {
    pub name: Identifier,
    pub arguments: Vec<FunctionArgument>,
    pub ret_type: Type,
    pub locals: Vec<VariableDeclaration>,
    pub body: Expression,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Procedure {
    pub name: Identifier,
    pub arguments: Vec<ProcedureArgument>,
    pub locals: Vec<VariableDeclaration>,
    pub body: Statement,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct FunctionArgument {
    pub name: Identifier,
    pub ty: Type,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct ProcedureArgument {
    pub modifier: ProcedureArgumentModifier,
    pub name: Identifier,
    pub ty: Type,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum ProcedureArgumentModifier {
    In,
    Out,
    InOut,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Expression {
    Var {
        name: Identifier,
    },
    Num {
        value: u64,
    },
    Index {
        base: Box<Expression>,
        index: Box<Expression>,
    },
    FunctionCall {
        base: Box<Expression>,
        arguments: Vec<Expression>,
    },
    Operator {
        op: Operator,
        arg_a: Box<Expression>,
        arg_b: Box<Expression>,
    },
    IfThenElse {
        cond: Box<Expression>,
        then_expr: Box<Expression>,
        else_expr: Box<Expression>,
    },
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Statement {
    Block {
        statements: Vec<Statement>,
    },
    Assignment {
        lhs: Expression,
        rhs: Expression,
    },
    Return {
        expr: Expression,
    },
    ProcedureCall {
        name: Identifier,
        arguments: Vec<(Option<ProcedureArgumentModifier>, Expression)>,
    },
    If {
        cond: Expression,
        then_stmt: Box<Statement>,
        else_stmt: Option<Box<Statement>>,
    },
    While {
        cond: Expression,
        body: Box<Statement>,
    },
    For {
        var: Identifier,
        initial_value: Expression,
        final_value: Expression,
        loop_type: ForLoopType,
        body: Box<Statement>,
    },
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Operator {
    Plus,
    Minus,
    Multiplication,
    Division,
    Modulus,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
    Equal,
    NotEqual,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct VariableDeclaration {
    pub name: Identifier,
    pub ty: Type,
    pub value: Expression,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Type {
    Named(Identifier),
    Parametrized {
        base: Box<Type>,
        parameters: Vec<TypeArgument>,
    },
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum TypeArgument {
    Type(Type),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum ForLoopType {
    UpTo,
    DownTo,
}
